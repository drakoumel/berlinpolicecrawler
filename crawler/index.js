var program = require('commander');
var ArgumentParser = require('argparse').ArgumentParser;
var colors = require('colors');
var c = require('./crawler');

var parser = new ArgumentParser({
    version: '0.0.1',
    addHelp:true,
    description: 'Polizei-Berlin on-demand crawler.'
  });

  parser.addArgument(
    [ '-url', '--url' ],
    {
      help: 'The seed url to start crawling'
    }
  );

  parser.addArgument(
    [ '-startPage', '--startPage' ],
    {
      help: 'The page index to start crawling from.'
    }
  );

  parser.addArgument(
    [ '-endPage', '--endPage' ],
    {
      help: 'The maximum page index it should crawling till.'
    }
  );

var args = parser.parseArgs();

if (args.url == null || args.url == undefined) {

    info("-url was not provided.")
    info("Defaulting -url to [https://www.berlin.de/polizei/polizeimeldungen]")
    args.url = 'https://www.berlin.de/polizei/polizeimeldungen';
} else {
    info("Will crawl: " + args.url)
}

function err(msg)
{
    console.log('⚠️  ' + msg.red);
}

function info(msg)
{
    console.log(msg.blue);
}

var seedURL = args.url;
var maxIndex = Number(args.endPage) || 99999;
var minIndex = Number(args.startPage) || 1 ;

c.crawl(seedURL, minIndex, maxIndex);