// start
const fs = require('fs');
const puppeteer = require('puppeteer');
var colors = require('colors');
const ora = require('ora');
var nl = (process.platform === 'win32' ? '\r\n' : '\n')
const spinner = ora('Crawling page... ');
let _ = require('lodash');
const {
    URL,
    URLSearchParams
} = require('url');

let db = { reports: [] };
const csv = require('csvtojson')
const winston = require('winston');

async function crawl(seedUrl, startPage, endPage) {
    console.log(startPage, endPage);

    try {
        const browser = await puppeteer.launch({
            headless: true
        });

        var pageIndex = startPage;

        const page = await browser.newPage();
        spinner.start();

        do {
            var uri = await navigateWithPagination(page, seedUrl, pageIndex)
            var items = await grabItemsFromPage(page);
            items = sanitizeItems(items);

            db.reports = db.reports.concat(items);

            header = false;
            pageIndex++;
            spinner.succeed();

        } while (await checkPagination(page, pageIndex, endPage))

        writeToFile(db);

        browser.close()
    } catch (e) {
        console.log('😱   Error! talk to your Developer.'.red)
        console.log(e);
    }
}

async function grabItemsFromPage(page) {
    var items = await page.evaluate(() => {
        const results = Array.from(document.querySelectorAll('.row-fluid'));
        return results.map(result => {
            return {
                url : result.getElementsByTagName('a')[0].href,
                datetime: result.querySelector('.date').innerText,
                title: result.querySelector('.text > a') ? result.querySelector('.text > a').innerText : '',
                location: result.querySelector('.text > span') ? result.querySelector('.text > span').innerText : '',
                description: '',
                no: '',                
            }
        });
    });

    return items;
}

async function navigateWithPagination(page, seedUrl, pageIndex) {
    var url = seedUrl;

    if (pageIndex > 1) {
        if (url.indexOf('?') != -1) {
            url += '&page_at_1_1=' + pageIndex;
        } else {
            url += '/?page_at_1_1=' + pageIndex;
        }
    }

    spinner.text = '[' + pageIndex + '] Crawling page... ' + url;
    spinner.stop();
    spinner.start();

    await page.goto(url, {
        waitUntil: 'networkidle2'
    });

    return url;
}

async function checkPagination(page, pageIndex, maxPageIndex) {
    if (pageIndex > maxPageIndex) {
        return false;
    }

    let value = await page.evaluate((sel) => {
        var val = document.querySelector(sel);
        return val ? val.innerText : false;
    }, '#top > div > div > div > div:nth-child(4) > div.span7.column-content > div.html5-section.article > div.html5-section.body > div.html5-section.modul-autoteaser.teaser > div.html5-nav > div > ul > li.pager-item.last.next > a')

    if (value == false) {
        return false;
    }

    if (pageIndex > Number(value)) {
        return false;
    }

    return true;
}


function sanitizeItems(items) {
    for (var i = 0; i < items.length; i++) {
        items[i].url = items[i].url.trim();
        items[i].datetime = items[i].datetime.replace('Uhr','').trim();
        items[i].title = items[i].title.trim();
        items[i].location = items[i].location.replace('Ereignisort:','').trim();
    }

    return items;
}

async function writeToFile(obj, callback) {
    var json = JSON.stringify(obj);
    fs.writeFile('output-' + getToday() + '.json', json, 'utf8', callback);
}


String.prototype.count = function (s1) {
    return (this.length - this.replace(new RegExp(s1, "g"), '').length) / s1.length;
}

function getToday()
{
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1;
    var yyyy = today.getFullYear();

    return dd + '-' + mm + '-' + yyyy;;
}

module.exports.crawl = crawl;