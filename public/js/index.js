function calculateColor(value) {
    return '#e2bafd';
}

var map;
var infoWindow;

function initMap() {

    map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: 52.5200,
            lng: 13.4050
        },
        zoom: 11
    });

    for (var i = 0; i < districts.length; i++) {
        var district = districts[i];
        var cords = [];
        var districtReports = reports.filter(report => report.location == district.properties.localname);

        for (var j = 0; j < district.geometry.coordinates.length; j++) {
            cords.push({
                lat: district.geometry.coordinates[j][1],
                lng: district.geometry.coordinates[j][0]
            });
        }

        // Construct the polygon.
        var districtPolygon = new google.maps.Polygon({
            paths: cords,
            strokeColor: '#9b15f9',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: calculateColor(districtReports.length),
            fillOpacity: 0.35
        });

        districtPolygon.set("Reports", districtReports);
        districtPolygon.set("DistrictName", district.properties.localname);

        districtPolygon.setMap(map);

        google.maps.event.addListener(districtPolygon, 'click', function (event) {
            document.getElementById('reportList').innerHTML = '';
            document.getElementById('location').innerText = this.DistrictName;
            var reportList = document.getElementById('reportList');

            for (var i = 0; i < this.Reports.length; i++) {
                var reportItem = this.Reports[i];

                var reportListItem = document.createElement("LI");
                reportListItem.className = "content";

                var reportListLink = document.createElement("A");
                reportListLink.href = reportItem.url;
                reportListLink.rel = "noopener";
                reportListLink.target = "_blank";
                reportListLink.innerHTML = reportItem.title;

                var reportListItemParagraph = document.createElement("P");
                reportListItemParagraph.className = "subtitle is-4";
                reportListItemParagraph.appendChild(reportListLink);

                var reportListItemSpan = document.createElement("SPAN")
                reportListItemSpan.className = "subtitle is-6";
                reportListItemSpan.innerHTML = "&nbsp;" + reportItem.datetime;
                reportListItemParagraph.appendChild(reportListItemSpan);

                reportListItem.appendChild(reportListItemParagraph);

                reportList.appendChild(reportListItem);
                reportList.appendChild(document.createElement("HR"));
            }

            document.getElementById('safe-card').className = "";
        });

        districtPolygon.addListener("mouseover", function () {
            this.setOptions({
                fillColor: "#00FF00"
            });
        });

        districtPolygon.addListener("mouseout", function () {
            this.setOptions({
                fillColor: calculateColor(districtReports.length)
            });
        });
    }
}

function submitSafeReport() {
    var inputbox = document.createElement('INPUT');
    safeReportContent.innerHTML = '<div class="field has-addons"> \
                                    <div class="control is-expanded"> \
                                        <input class="input is-medium" type="text" placeholder="Enter your email and we will email you the results back!"> \
                                    </div>\
                                    <div class="control">\
                                        <button type="button" onclick="sendEmail()" class="button is-info is-medium"> Get my results! </a> \
                                        </div>\
                                    </div>';

    document.getElementById('footerSubmit').className += " hidden";
}

function sendEmail()
{
    //slide-right
    safeReportContent.innerHTML = "<h2 class=''>You will receive the results at the end of the week! 🤗</h2>"
}